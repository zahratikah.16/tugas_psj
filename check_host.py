import subprocess
import sys
import re


# Fungsi untuk mencetak pesan, baik gagal, berhasil atau ada kesalahan lain
def print_message(args=True, host='none', invalidIp=False):
    if not args:
        sys.exit('Gagal: IP address belum diberikan\ncontoh : check_host.py 192.168.6.67')
    elif args == 'Up' or args == 'Down':
        sys.exit('Host ' + host + ' is ' + args)
    elif invalidIp:
        sys.exit('Alamat ip salah')
    else:
        sys.exit("Ada kesalahan lain")


# Regex
regex = "^((25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9]?[0-9])\.){3}(25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9]?[0-9])$"


def main(arg):
    # Cek jika ip yang di masukan itu benar
    if re.search(regex, arg):
        p1 = subprocess.Popen(['ping', '-c3', arg], stdout=subprocess.PIPE)
        p2 = subprocess.Popen(['grep', 'packet'], stdin=p1.stdout, stdout=subprocess.PIPE)
        p1.stdout.close()
        output = p2.communicate()[0].decode('utf-8').split(",")
        if len(output) == 5:
            print_message(args='Down', host=arg)
        else:
            if int(output[2][1]) >= 1:
                print_message(args='Down', host=arg)
            else:
                print_message(args='Up', host=arg)
    else:
        print_message(invalidIp=True)


if _name_ == '_main_':
    if len(sys.argv) == 2:
        main(sys.argv[1])
    else:
        print_message(args=False)
